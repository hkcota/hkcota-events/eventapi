<?php

use Drupal\views\ViewExecutable;

/**
 * @file
 * A module for Stripe related Form API integrations.
 *
 * PHP Version 7
 *
 * @category drupal-module
 * @package Drupal
 * @subpackage Drupal\eventapi
 * @author Koala Yeung <koalay@gmail.com>
 * @license https://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link https://gitlab.com/hkcota/hkcota-events/eventapi
 */

/**
 * Implements hook_theme().
 */
function eventapi_theme($existing, $type, $theme, $path) {
  $path .= '/templates';
  return [
    'eventapi_timetable_preview' => [
      'path' => $path,
      'variables' => [
        'wrapper_attributes' => null,
        'conference' => null,
        'days' => [],
      ],
    ],
  ];
}

/**
* Implements hook_views_pre_render().
*/
function eventapi_views_pre_render(ViewExecutable $view) {
  if (!isset($view)) {
    return;
  }
  switch ($view->storage->id()) {
    case 'conferences':
      $view->element['#attached']['library'][] = 'eventapi/conference-card';
      break;
    case 'person_related_events':
      $view->element['#attached']['library'][] = 'eventapi/event-card';
      break;
    case 'people':
      $view->element['#attached']['library'][] = 'eventapi/person-card';
      break;
  }
}
