# HKCOTA Event API

A simple API module to serve website contents (events, venues, organization)
with API. This is to provide a simple interface to input and store event
information. The aim for this module is to facilitate the participation of
development and maintenance of statically generated websites (of event such
as HKOSCon).

Please note that the module is heavily based on the Drupal content type
setup (i.e. taxonomy, field) of [the HKCOTA Event server][hkcota-event-server].


[hkcota-event-server]: https://events.cota.hk


## API Calls

API is provided to review event informations.

The API endpoint works by searching the event's Taxonomy Term by its name. For
example if the event's name is "HKOSCon 2023", the {eventTerm} should be the
url encoded string of "HKOSCon 2023" (i.e. "HKOSCon%202023").

Alternatively, the {eventTerm} can also accept alias path or the taxonomy term
id of the same term.

All response are in "application/json; charset=utf-8" format.

### API Endpoints

* `/api/v1`

  Returns all API endpoints that is not entity specific (i.e. only listing
  endpoints like "conferenceyear" is here).

* `/api/v1/conferenceyear`

  Returns an array of all the conference years, along with their relevant
  endpoints and the event counts in them.

* `/api/v1/info/{eventTerm}`

  Returns the basic information about the event, includes the different tiers of
  sponsors, the patreons' name (if there is any).

* `/api/v1/events/{eventTerm}`

  Returns an array of events of the event.

* `/api/v1/days/{eventTerm}`

  Returns an array of days, each with the timeslots (a.k.a. events) in it. Very
  useful in generating the event timetable.

## License

This module is licensed under the MIT License. A copy of the license
can be obtained with the source code [here](LICENSE.md).
