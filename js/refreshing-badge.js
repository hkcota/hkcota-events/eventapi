// For all images with class .eventapi-refreshing-badge, refresh every 30 seconds
(function (Drupal, once) {
  const refreshInterval = 30 * 1000; // 30 x 1000 milliseconds

  /**
   *
   * @var {Number} interval - Interval, in terms of number of millisecond(s), for the image to reload.
   * @var {HTMLImageElement} img - The image element to be set to refresh automatically.
   */
  function setSelfRefresh(interval, img) {
    if (!(img instanceof HTMLImageElement)) {
      console.warn('element is not HTMLImageElement, abort', img);
      return;
    }
    const src = new URL(img.src);
    window.setInterval(() => {
      const params = new URLSearchParams(src.search);
      params.set('timestamp', new Date().getTime());
      src.search = params.toString();
      img.src = src.toString();
    }, interval);
  }

  Drupal.behaviors.eventapiRefreshingBadge = {
    attach: function (context, settings) {
      const badges = once('eventapiRefreshingBadge', 'img.eventapi-refreshing-badge', context)
      badges.forEach(setSelfRefresh.bind(this, refreshInterval));
    },
  };
})(Drupal, once);
