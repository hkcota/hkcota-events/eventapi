<?php

namespace Drupal\eventapi\Traits;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * Trait to render a given FieldItem / FieldItemList implementation.
 */
trait FieldRendererTrait
{
  /**
   * Render a given field item interface into proper HTML.
   *
   * Also applied standard sanitazations (e.g. strip all tags other than <p>, <br>, <a>.
   * Trim space and linebreaks from beginning and end).
   *
   * @param FieldItemInterface|FieldItemListInterface $field The field item to render.
   * @param string|array $display_options The display option for renderering. See FieldItemInterface::view().
   * @param string $allowed_tags To instruct strip_tags() of the tags to be preserved in the resulting HTML string.
   *
   * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Field!FieldItemInterface.php/function/FieldItemInterface%3A%3Aview/8.9.x
   * @see https://www.php.net/manual/en/function.strip-tags.php
   *
   * @return
   */
  protected function renderField(
    FieldItemInterface|FieldItemListInterface $field,
    string|array $display_options='full',
    string $allowed_tags='<p><a><br><ul><ol><li><dd><dt><dl><em><strong>',
  ): string
  {
    if ($field->isEmpty()) {
      return '';
    }
    $renderArray = $field->view($display_options);
    return trim(
      strip_tags(
        (string) $this->getRenderer()->render($renderArray),
        $allowed_tags,
      ),
      "\t\r\n ",
    );
  }

  /**
   * Return the renderer given to this instance.
   *
   * @return RendererInterface $renderer
   */
  abstract function getRenderer(): RendererInterface;
}
