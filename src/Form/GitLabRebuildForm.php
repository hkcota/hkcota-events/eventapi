<?php

namespace Drupal\eventapi\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

class GitLabRebuildForm extends FormBase {

  public const DefaultMessage = 'Manually trigger rebuild from Event API Server';
  public const DefaultBranch = 'main';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $build_options = reset($form_state->getBuildInfo()['args']);

    /** @var \Drupal\taxonomy\Entity\Term */
    $term = $build_options['term'];
    $branch_options = static::getBranchOptions($term);
    $branches = array_keys($branch_options);

    // Generate badges array for display
    $badges = array_map(fn ($branch) => [
      'name' => $branch,
      'href' => "https://gitlab.com/{$build_options['project_id']}/-/pipelines?" . http_build_query([
        'scope' => 'all',
        'ref' => $branch
      ]),
      'image_url' => "https://gitlab.com/{$build_options['project_id']}/badges/{$branch}/pipeline.svg?" . http_build_query([
        'key_text' => $branch,
      ]),
    ], $branches);

    // Usage message about what the form is
    $form['usage'] = [
      '#type' => 'inline_template',
      '#template' => '<div style="margin: 1em 0;">{% trans %}You may use this form to manually trigger website rebuild{% endtrans %}</div>',
    ];

    // Show GitLab branches pipeline status
    $form['badges'] = [
      '#type' => 'inline_template',
      '#template' => <<<HTML
        {% if badges %}
          <ul class="gitlab-badges">
            {% for badge in badges %}
              <li class="gitlab-badges-badge">
                <a href="{{ badge.href }}" target="_blank">
                  <img
                    class="eventapi-refreshing-badge"
                    loading="lazy"
                    src="{{ badge.image_url }}"
                    alt="{{ badge.name }}"
                    title="{% trans %}Click to go to GitLab and see pipeline status of the branch: {{ badge.name }}{% endtrans %}"
                  >
                </a>
              </li>
            {% endfor %}
          </ul>
        {% endif %}
      HTML,
      '#context' => [
        'badges' => $badges,
      ],
    ];

    // Select branch
    $form['branch'] = [
      '#type' => 'select',
      '#title' => $this->t('Target Branch'),
      '#description' => $this->t('Branch to trigger pipeline for.'),
      '#options' => $branch_options,
      '#placeholder' => $this->t('Example: main'),
      '#default_value' => $branches[0],
      '#required' => true,
    ];

    // Build message to supply to pipeline
    $form['message'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Will be used as commit message when pushing to GitHub pages.'),
      '#title' => $this->t('Build Message'),
      '#placeholder' => static::DefaultMessage,
    ];

    $form['project_id'] = [
      '#type' => 'token',
      '#value' => $build_options['project_id'],
    ];
    $form['project_token'] = [
      '#type' => 'token',
      '#value' => $build_options['project_token'],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Rebuild'),
    ];
    $form['#attached']['library'][] = 'eventapi/form.gitlab-rebuild-form';
    $form['#cache'] = [
      'max-age' => 0,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eventapi_gitlab_rebuild_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $build_options = reset($form_state->getBuildInfo()['args']);

    /** @var \Drupal\user\Entity\User */
    $user = $build_options['user'];
    $username = ucfirst($user->getAccountName());
    $email = $user->getEmail();
    $message = $form_state->getValue('message') ?: static::DefaultMessage;

    $branch = $form_state->getValue('branch') ?: 'main';
    $project_id = $form_state->getValue('project_id');
    $project_token = $form_state->getValue('project_token');
    $message = $form_state->getValue('message') ?: static::DefaultMessage;

    // used path stug to construt Travis API (v3) endpoint
    $path_stug = rawurlencode($project_id);

    // api context
    $context = stream_context_create([
      'http' => [
        'method' => 'POST',
        'header' => implode("\r\n", [
          'Content-Type: application/x-www-form-urlencoded',
          'Accept: application/json',
        ]),
        'content' => http_build_query([
          'ref' => $branch,
          'token' => $project_token,
          'variables[GIT_USER_NAME]' => $username,
          'variables[GIT_USER_EMAIL]' => $email,
          'variables[GIT_COMMIT_MESSAGE]' => $message,
        ]),
      ],
    ]);
    \Drupal::logger('eventapi')->debug('Triggering pipeline through GitLab API endpoint: @url', [
      '@url' => "https://gitlab.com/api/v4/projects/{$path_stug}/trigger/pipeline",
    ]);
    $response = file_get_contents(
      "https://gitlab.com/api/v4/projects/{$path_stug}/trigger/pipeline",
      false,
      $context
    );
    $response_header = array_reduce($http_response_header, function ($carry, $line) {
      if (preg_match('~^(HTTP)/(\d+\.\d+) (\d+) (.+?)$~', $line, $matches)) {
        $carry['protocol'] = $matches[1];
        $carry['protocol_version'] = $matches[2];
        $carry['status_code'] = $matches[3];
        $carry['status'] = $matches[4];
      } else if (preg_match('~^(.+?): (.+?)$~', $line, $matches)) {
        $carry[$matches[1]] = $matches[2];
      }
      return $carry;
    });

    // Add debug logging of the API response.
    \Drupal::logger('eventapi')->debug('GitLab API response: @response', [
      '@response' => $response,
    ]);

    if (preg_match('~^20\d$~', $response_header['status_code'])) {
      \Drupal::messenger()->addStatus(Markup::create(sprintf(
        'Started rebuilding %s. Please go to %s for details.'.
        '<details style="margin-top: 0.8em"><summary style="display: list-item; cursor: pointer">Raw API Response</summary>'.
        '<div style="background-color: rgba(255,255,255,0.6); padding: 1em;">%s</div></details>',
        $branch,
        Link::fromTextAndUrl(
          'GitLab',
          Url::fromUri(
            'https://gitlab.com/' . $project_id . '/-/pipelines',
            ['attributes' => ['target' => '_blank']]
          )
        )->toString(),
        $response
      )));
    } else {
      \Drupal::messenger()->addError(sprintf('Failed to trigger rebuild for branch %s. Status code: %s', $branch, $response_header['status_code']));
      \Drupal::messenger()->addError(sprintf("failed to submit rebuild request for $project_id: $response"));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty(trim($form_state->getValue('project_id')))) {
      $form_state
        ->setErrorByName('project_id', $this
          ->t('No project_id was specified in the form\'s build info.'));
    }
    if (empty(trim($form_state->getValue('project_token')))) {
      $form_state
        ->setErrorByName('project_token', $this
          ->t('No project_token was specified in the form\'s build info.'));
    }

    // Check if the branch specified is allowed.
    $build_options = reset($form_state->getBuildInfo()['args']);
    $term = $build_options['term'];
    $branch_options = static::getBranchOptions($term);
    if (!in_array($form_state->getValue('branch'), $branch_options)) {
      $form_state
        ->setErrorByName('branch', $this
          ->t('Branch specified is not allowed: @branch. Allowed branches: @allowed', [
            '@branch' => $form_state->getValue('branch'),
            '@allowed' => implode(', ', $branch_options),
          ]));
    }
  }

  private static function getBranchOptions(Term $term)
  {
    $branches = array_map(fn($value) => $value['value'], $term->field_gitlab_branches->getValue());
    return array_combine($branches, $branches);
  }
}
