<?php

namespace Drupal\eventapi\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

define('EVENTAPI_DEFAULT_MESSAGE', 'Manually trigger rebuild from Event API Server');

class TravisRebuildForm extends FormBase {

  private $_travis_id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $build_options = reset($form_state->getBuildInfo()['args']);
    $form['message'] = [
      '#type' => 'textfield',
      '#title_display' => 'invisible',
      '#title' => $this->t('Build Message'),
      '#placeholder' => $this->t('Build Message') . '. Default: ' . EVENTAPI_DEFAULT_MESSAGE,
    ];
    $form['travis_id'] = [
      '#type' => 'token',
      '#value' => $build_options['travis_id'],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Rebuild'),
    ];
    $form['#cache'] = [
      'max-age' => 0,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eventapi_travis_rebuild_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $username = ucfirst(Drupal::currentUser()->getUsername());

    $travis_id = $form_state->getValue('travis_id');
    $travis_token = getenv('TRAVIS_TOKEN');
    $message = $form_state->getValue('message') ?: EVENTAPI_DEFAULT_MESSAGE;

    // used path stug to construt Travis API (v3) endpoint
    $path_stug = rawurlencode($travis_id);

    // api context
    $context = stream_context_create([
      'http' => [
        'method' => 'POST',
        'header' => implode("\r\n", [
          'Content-Type: application/json',
          'Accept: application/json',
          'Travis-API-Version: 3',
          'Authorization: token ' . $travis_token,
        ]),
        'content' => json_encode([
          'request' => [
            'message' => sprintf('%s: %s', $username, $message),
            'branch' => 'master',
          ],
        ]),
      ],
    ]);
    $response = file_get_contents(
      "https://api.travis-ci.org/repo/{$path_stug}/requests",
      false,
      $context
    );
    $response_header = array_reduce($http_response_header, function ($carry, $line) {
      if (preg_match('~^(HTTP)/(\d+\.\d+) (\d+) (.+?)$~', $line, $matches)) {
        $carry['protocol'] = $matches[1];
        $carry['protocol_version'] = $matches[2];
        $carry['status_code'] = $matches[3];
        $carry['status'] = $matches[4];
      } else if (preg_match('~^(.+?): (.+?)$~', $line, $matches)) {
        $carry[$matches[1]] = $matches[2];
      }
      return $carry;
    });

    if (preg_match('~^20\d$~', $response_header['status_code'])) {
      drupal_set_message(Markup::create(sprintf(
        'Rebuild started. Please go to %s for details.'.
        '<details style="margin-top: 0.8em"><summary style="display: list-item; cursor: pointer">Raw API Response</summary>'.
        '<div style="background-color: rgba(255,255,255,0.6); padding: 1em;">%s</div></details>',
        Drupal::l(
          'Travis CI',
          Url::fromUri(
            'https://travis-ci.org/' . $travis_id,
            ['attributes' => ['target' => '_blank']]
          )
        ),
        $response
      )));
    } else {
      drupal_set_message(sprintf('status code: %s', $response_header['status_code']), 'error');
      drupal_set_message(sprintf("failed to submit rebuild request: $response", 'error'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty(trim($form_state->getValue('travis_id')))) {
      $form_state
        ->setErrorByName('travis_id', $this
          ->t('No travis_id was specified in the form\'s build info.'));
    }
    if (empty(getenv('TRAVIS_TOKEN'))) {
      $form_state
        ->setErrorByName('travis_id', $this
          ->t('No TRAVIS_TOKEN is set to environment variable.'));
    }
  }
}
