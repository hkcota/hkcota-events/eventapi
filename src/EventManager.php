<?php

namespace Drupal\eventapi;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\eventapi\Traits\FieldRendererTrait;
use Drupal\image\ImageStyleInterface;
use Drupal\node\NodeInterface;
use Drupal\node\NodeStorageInterface;
use Drupal\path_alias\AliasManager;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorageInterface;

class EventManager
{
  use FieldRendererTrait;

  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected AliasManager $pathAliasManager,
    protected LanguageManagerInterface $languageManager,
    protected RendererInterface $renderer,
    protected FileUrlGeneratorInterface $fileUrlGenerator,
  ) {
  }

  /**
   * Method to retrieve term data from a given search string.
   *
   * @param string $search
   *   Either the tid, the alias path name or the exact term
   *   name of a conference year term.
   *
   * @return Term|null
   *   The term result or null, if not found.
   */
  public function searchConferenceYear(string $search): ?Term
  {
    // check if the term name is an alias
    $path = $this->pathAliasManager->getPathByAlias('/conferenceyear/' . $search);
    if (is_numeric($search)) {
      $term = Term::load($search);
    } elseif (preg_match('/taxonomy\/term\/(\d+)/', $path, $matches)) {
      $term = Term::load($matches[1]);
    } else {
      // getting term by term name, if any
      $terms = $this->getTermStorage()
        ->loadByProperties([
          'vid' => 'conference_year', // hard coded for now
          'name' => $search,
        ]);
      $term = reset($terms);
    }
    return ($term !== false) ? $term : null;
  }

  /**
   * Get an array of all conference year taxonomy
   * term inforamtions.
   *
   * @return array
   */
  public function getConferenceYears(): array
  {
    // getting term by term name, if any
    $termStorage = $this->getTermStorage();
    $tree = $termStorage->loadTree('conference_year');

    // map more data for each term
    return array_map(function ($termInfo) use ($termStorage) {
      $term = $termStorage->load($termInfo->tid);
      $count = $this
        ->getNodeStorage()
        ->getQuery()
        ->accessCheck(TRUE)
        ->condition('status', 1)
        ->condition('type', 'event')
        ->condition('field_conference', $termInfo->tid)
        ->count()
        ->execute();
      $termInfo->uuid = $term->uuid();
      $termInfo->eventCount = $count;
      return $termInfo;
    }, $tree);
  }

  /**
   * Method to retrieve all events in the given conference day.
   *
   * @param Term $term
   *   The term object to get relevant days from.
   *
   * @return array
   *   A array of events.
   */
  public function getEvents(
    Term $term,
    array $options=[]
  ): array
  {
    $langcode = $options['lang'] ?? $this->languageManager->getCurrentLanguage()->getID();
    $is_admin = (bool) ($options['is_admin'] ?? false);
    $tids = array_map(function ($item) {
      return $item->tid;
    }, $this->getChildrenTermsOf($term));
    $tids[] = $term->tid->value; // append the term's tid
    $query = $this->getNodeStorage()
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', 1)
      ->condition('type', 'event')
      ->condition('field_conference', $tids, 'IN');
    $entity_ids = $query->execute();
    $nodes = $this->getNodeStorage()->loadMultiple($entity_ids);
    $events = array_values(array_map($this->mapEvent($langcode, true, $is_admin), $nodes));
    $dates = [];
    for ($i = 0; $i < sizeof($events); $i++) {
      $dates[$events[$i]['time']['date']][] = $i;
    }
    ksort($dates);
    foreach (array_keys($dates) as $day => $date) {
      $day += 1;
      foreach ($dates[$date] as $i) {
        $events[$i]['time']['day'] = $day;
      }
    }
    return $events;
  }

  /**
   * Method to retrieve days information for building
   * timetable accordingly.
   *
   * @param Term $term
   *   The term object to get relevant days from.
   * @param string $langcode
   *   The language id of the Drupal language to use.
   * @param bool $recursive
   *   If the days should search for events of child terms of
   *   the specified term. Default: false.
   *
   * @return array
   *   A structured array of events in different days.
   */
  public function getDays(
    Term $term,
    ?string $langcode=null,
    bool $recursive=false,
  )
  {
    if ($langcode === null) {
      $language = $this->languageManager->getCurrentLanguage();
      $langcode = $language->getID();
    }

    // Only include child terms if $recursive flag is true
    $tids = $recursive
      ? array_map(fn ($item) => $item->tid, $this->getChildrenTermsOf($term))
      : [];
    $tids[] = $term->tid->value; // append the term's tid

    $query = $this->getNodeStorage()->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', 1)
      ->condition('type', 'event')
      ->condition('field_conference', $tids, 'IN')
      ->sort('field_venue.entity.weight')
      ->sort('field_timeslots');
    $entity_ids = $query->execute();
    $nodes = $this->getNodeStorage()->loadMultiple($entity_ids);
    $events = array_map($this->mapEvent($langcode, TRUE), $nodes);
    $days = array_reduce($nodes, self::reduceDaysNTimeslots($events), array());
    ksort($days); // Sort days by key
    $days = self::sortDaysTimeslots($days);
    return self::compressTimeslotLevels($days);
  }

  /**
   * Method to retrieve all news about a given conference.
   *
   * @param Term $term
   *   The term object to get relevant days from.
   *
   * @return array
   *   A array of events.
   */
  public function getNews(Term $term, ?string $langcode = null): array
  {
    if ($langcode === null) {
      $language = $this->languageManager->getCurrentLanguage();
      $langcode = $language->getID();
    }
    $tids = array_map(function ($item) {
      return $item->tid;
    }, self::getChildrenTermsOf($term));
    $tids[] = $term->tid->value; // append the term's tid
    $query = $this->getNodeStorage()->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', 1)
      ->condition('type', 'article')
      ->condition('field_conference', $tids, 'IN')
      ->sort('created' , 'DESC');
    $entity_ids = $query->execute();
    $nodes = $this->getNodeStorage()->loadMultiple($entity_ids);
    return array_map($this->mapNews($langcode), $nodes);
  }

  /**
   * Map event entity with inner fields
   *
   * @param Entity $node
   *   Entity object loaded with entity api.
   * @param boolean $map_timeslots
   *   Whether to map "timeslots" to the event entity.
   */
  protected function mapEvent(
    string $langcode,
    bool $map_timeslots=false,
    bool $is_admin=false
  ) {
    return function (NodeInterface $node) use ($langcode, $map_timeslots, $is_admin) {
      // TODO: learn if the event is topic or not.
      // TODO: use different structure for non-topic events.
      if ($node->hasTranslation($langcode)) {
        $node = $node->getTranslation($langcode);
      }

      // venue
      $venue = ['name' => '', 'weight' => 0];
      foreach ($node->get('field_venue') as $venue) {
        /** @var TermInterface */
        $venue_entity = $venue->entity;
        $venue = [
          'uuid' => $venue_entity->uuid(),
          'name' => $venue_entity->name->value,
          'weight' => (int) $venue_entity->getWeight(),
        ];
        break;
      }

      // language
      $language = '';
      foreach ($node->get('field_language') as $language) {
        $language = $language->entity->name->value;
        break;
      }

      // language (simultaneous interpretation)
      $language_sym_interp = [];
      foreach ($node->get('field_language_sym_interp') as $language_sym) {
        $language_sym_interp[] = $language_sym->entity->name->value;
      }

      // level
      $level = '';
      foreach ($node->get('field_level') as $level) {
        $level = $level->entity->name->value;
        break;
      }

      // speakers
      $speakers = [];
      foreach ($node->get('field_speaker') as $speaker) {
        /** @var NodeInterface */
        $speaker_entity = $speaker->entity;
        $thumbnail = isset($speaker_entity->get('field_image')[0]) ?
          $speaker_entity->get('field_image')[0]->entity : false;
        $thumbnail_url = ($thumbnail)
          ? $this->fileUrlGenerator->generateAbsoluteString($thumbnail->getFileUri())
          : '';
        $speakers[] = [
          'uuid' => $speaker_entity->uuid(),
          'name' => (string) $speaker_entity->title->value,
          'community' => !empty($speaker_entity->field_affiliations) ? (string) $speaker_entity->field_affiliations->value : '',
          'country' => (string) ($speaker_entity->get('field_country')[0]->entity->name->value ?? null),
          'description' => $this->renderField($speaker_entity->body),
          'thumbnail' => $thumbnail_url,
        ];
      }
      $speaker = !empty($speakers) ? $speakers[0] : NULL;

      // video recordings
      $video_recordings = [];
      foreach ($node->get('field_video_recordings') as $video_recording) {
        $view = $video_recording->view();
        $view['children']['#query']['autoplay'] = false;
        $video_recordings[] = [
          'url' => $video_recording->value,
          'html' => $this->renderer->render($view),
        ];
      }

      $live_accessories = [
        'hackmd' => static::getFieldUrlString($node, 'field_hackmd_url'),
        'slido' => static::getFieldUrlString($node, 'field_slido'),
        'survey' => static::getFieldUrlString($node, 'field_survey_url'),
        'freenode_irc' => ($node->get('field_freenode_irc_channel')->count() > 0)
          ? $node->get('field_freenode_irc_channel')->first()->value : '',
        'liberachat_irc' => ($node->get('field_librechat_irc_channel')->count() > 0)
          ? $node->get('field_librechat_irc_channel')->first()->value : '',
        'qna_url' => static::getFieldUrlString($node, 'field_qna_url'),
      ];

      // video streaming
      $video_streamings = [];
      foreach ($node->get('field_video_streaming_reference') as $video) {
        $video_streaming_node = $video->entity;
        $video_streaming = [
          'language' => isset($video_streaming_node->get('field_language')->first()->entity)
            ? $video_streaming_node->get('field_language')->first()->entity->label()
            : 'Default',
          'youtube' => $video_streaming_node->get('field_video_streaming')->value,
        ];
        if ($is_admin === TRUE) {
          $video_streaming['streaming_key'] =
            $video_streaming_node->get('field_video_streaming_key')->value;
        }
        $video_streamings[] = $video_streaming;
      }

      // slides
      $slides = [];
      foreach ($node->get('field_slides_url') as $item) {
        $slides[] = [
          'title' => $item->title,
          'url' => $item->getUrl()->toString(),
        ];
      }
      foreach ($node->get('field_slides_file') as $item) {
        $slides[] = [
          'title' => $item->description,
          'url' => $item->entity->createFileUrl(),
        ];
      }

      // individual event outputs
      $event = [
        'id' => (int) $node->id(),
        'uuid' => $node->uuid(),
        'topic' => ($node->field_is_topic->value === "1"),
        'remote_presentation' => ($node->field_is_remote->value === "1"),
        'display' => $node->title->value,
        'description' => $this->renderField($node->body),
        'venue' => $venue,
        'language' => $language,
        'language_sym_interp' => $language_sym_interp,
        'level' => $level,
        'speakers' => $speakers,
        'video_streamings' => $video_streamings ?? [],
        'live_accessories' => $live_accessories ?? [],
        'slido' => $slido[0] ?? NULL,
        'video_recordings' => $video_recordings,
        'slides' => $slides,
        'internal' => Url::fromUri("internal:/node/{$node->nid->value}")
          ->setAbsolute()->toString(),
      ];

      // map timeslots to the event entity
      $utc = new \DateTimezone('UTC');
      $hkt = new \DateTimezone('Asia/Hong_Kong');
      if ($map_timeslots) {
        $event['time'] = [];
        $event['timeslots'] = [];
        foreach ($node->get('field_timeslots') as $item) {
          $value = $item->getValue();
          $event['timeslots'][] = [
            'date' => (new \DateTime($value['value'], $utc))
              ->setTimezone($hkt)->format('Y-m-d'),
            'startTime' => (new \DateTime($value['value'], $utc))
              ->setTimezone($hkt)->format('H:i'),
            'endTime' => (new \DateTime($value['end_value'], $utc))
              ->setTimezone($hkt)->format('H:i'),
          ];
        }
        if (!empty($event['timeslots'])) {
          $l = sizeof($event['timeslots']) - 1;
          $event['time'] = [
            'date' => $event['timeslots'][0]['date'],
            'startTime' => $event['timeslots'][0]['startTime'],
            'endTime' => $event['timeslots'][$l]['endTime'],
          ];
        }
      }

      return $event;
    };
  }

  /**
   * Map News article entities with inner fields.
   *
   * @param Entity $node entity object loaded with entity api.
   */
  public static function mapNews(string $langcode)
  {
    return function (NodeInterface $node) use ($langcode) {
      if ($node->hasTranslation($langcode)) {
        $node = $node->getTranslation($langcode);
      }
      // individual news outputs
      return [
        'uuid' => $node->uuid(),
        'title' => $node->title->value,
        'description' => $this->renderField($node->body),
        'image' => $node->field_image->value,
        'date' => \DateTime::createFromFormat('U T', $node->getCreatedTime() . ' UTC')
          ->setTimezone(new \DateTimeZone('Asia/Hong_Kong'))
          ->format('c'),
        'internal' => Url::fromUri("internal:/node/{$node->nid->value}")
          ->setAbsolute()->toString(),
      ];
    };
  }

  /**
   * Render sponsor tiers into a structured array for API output.
   *
   * @param Term $term
   * @param array $sponsor_tiers
   * @param ImageStyleInterface|null|null $logo_image_style
   *
   * @return array
   */
  public function renderSponsors(
    Term $term,
    array $sponsor_tiers,
    ImageStyleInterface|null $logo_image_style = null,
  ): array
  {
    $keys = $sponsor_tiers;
    $values = array_map(function ($tier) use ($term, $logo_image_style) {
      $sponsor_tier = [
        'name' => ucfirst(preg_replace('/_plus$/', '+', $tier)),
        'sponsors' => [],
      ];
      foreach ($term->{"field_sponsors_$tier"} as $sponsor) {
        /** @var NodeInterface */
        $entity = $sponsor->entity;
        $sponsor_tier['sponsors'][] = $this->renderOrgView($entity, $logo_image_style);
      }
      return $sponsor_tier;
    }, $sponsor_tiers);

    $sponsors = array_combine($keys, $values);
    return array_filter($sponsors, fn($tier) => !empty($tier['sponsors']));
  }

  /**
   * Render organization entity view
   *
   * @param EntityInterface $entity Supposedly organization
   *
   * @return array
   */
  public function renderOrgView(
    EntityInterface $entity,
    ImageStyleInterface|null $logo_image_style = null,
  ): array
  {
    $logo = isset($entity->get('field_image')[0])
      ? $entity->get('field_image')[0]->entity : false;
    $logo_url = ($logo)
      ? (!empty($logo_image_style) ? $logo_image_style->buildUrl($logo->getFileUri()) : '')
      : '';

    $links = [];
    foreach ($entity->field_url as $field_url) {
      $links[] = [
        'title' => $field_url->title,
        'url' => $field_url->getUrl()->toString(),
      ];
    }

    return [
      'uuid' => $entity->uuid(),
      'title' => $entity->title->value,
      'logo' => $logo_url,
      'description' => $this->renderField($entity->body),
      'links' => $links,
    ];
  }

  /**
   * Get children Term(s) of a given Term.
   *
   * @param Drupal\taxonomy\Entity\Term $term
   *        Term entity to search children for.
   *
   * @return array
   *        Array of term data in object, but not Term class.
   */
  public function getChildrenTermsOf(Term $term)
  {
    return $this->getTermStorage()
      ->loadTree($term->bundle(), $term->tid->value);
  }

  /**
   * Get node storage.
   *
   * @return NodeStorageInterface
   */
  private function getNodeStorage(): NodeStorageInterface
  {
    return $this->entityTypeManager->getStorage('node');
  }

  /**
   * Get term storage.
   *
   * @return TermStorageInterface
   */
  private function getTermStorage(): TermStorageInterface
  {
    return $this->entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * Get the URL string from a supposedly Link field
   *
   * @return string
   */
  protected static function getFieldUrlString($node, string $field_name): string
  {
    return ($node->get($field_name)->count() > 0)
      ? $node->get($field_name)->first()->getUrl()->toString()
      : '';
  }

  /**
   *
   * @param array $events and array of mapped event assocs.
   */
  protected static function reduceDaysNTimeslots($events)
  {
    return function (array $carry, EntityInterface $node) use ($events) {
      foreach ($node->field_timeslots as $timeslot) {
        $range = self::parseDateRangeItem($timeslot);
        $date_str = $range['start']->format('Y-m-d');
        $start_time_str = $range['start']->format('H:i');
        $end_time_str = $range['end']->format('H:i');
        $carry[$date_str][$start_time_str][$end_time_str][] = $events[$node->nid->value];
      }
      return $carry;
    };
  }

  /**
   * Sort the days' timeslot by venue and weight.
   *
   * @param array $days
   *
   * @return array
   */
  protected static function sortDaysTimeSlots(array $days): array
  {
    foreach ($days as $date => $day) {
      foreach (array_keys($day) as $startTime) {
        foreach (array_keys($day[$startTime]) as $endTime) {
          usort($days[$date][$startTime][$endTime], function ($a, $b) {
            if (!isset($a['venue']) || !isset($b['venue'])) {
              return 0;
            }
            if ($a['venue']['weight'] != $b['venue']['weight']) {
              return $a['venue']['weight'] <=> $b['venue']['weight'];
            }
            return $a['venue']['name'] <=> $b['venue']['name'];
          });
        }
      }
    }
    return $days;
  }

  /**
   * Compress timeslot levels to a single level.
   *
   * @param array $day array of days[startTime][endTime]events hierarchy structure
   * @return array
   */
  protected static function compressTimeslotLevels(array $days): array
  {
    $result = [];
    $dayID = 0;
    foreach ($days as $date => $day) {
      $timeslots = [];
      foreach ($day as $start_time => $items) {
        foreach ($items as $end_time => $events) {
          $timeslots[] = [
            'startTime' => $start_time,
            'endTime' => $end_time,
            'events' => $events,
          ];
        }
      }
      usort($timeslots, function ($a, $b) {
        if ($a['startTime'] == $b['startTime']) return 0;
        return ($a['startTime'] < $b['startTime']) ? -1 : 1;
      });
      $result[$dayID] = [
        'day' => $dayID + 1,
        'date' => $date,
        'timeslots' => $timeslots,
      ];

      $dayID++;
    }
    return $result;
  }

  /**
   * Parse DateRangeItem object to DateTime objects.
   *
   * @param DateRangeItem $range
   * @param string $timezone
   *
   * @return array Assoc array with 2 terms:
   *  - start: \DateTime
   * - end: \DateTime
   */
  protected static function parseDateRangeItem(DateRangeItem $range, $timezone='Asia/Hong_Kong'): array
  {
    $tz = new \DateTimeZone($timezone);
    $start = \DateTime::createFromFormat('Y-m-d\TH:i:s T', $range->value . ' UTC');
    $start->setTimezone($tz);
    $end = \DateTime::createFromFormat('Y-m-d\TH:i:s T', $range->end_value . ' UTC');
    $end->setTimezone($tz);
    return [
      'start' => $start,
      'end' => $end,
    ];
  }

  /**
   * Return the renderer given to this instance.
   *
   * @return RendererInterface $renderer
   */
  protected function getRenderer(): RendererInterface
  {
    return $this->renderer;
  }
}
