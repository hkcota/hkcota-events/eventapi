<?php

namespace Drupal\eventapi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\eventapi\EventManager;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

class APIControllerV1 extends ControllerBase
{

  /**
   * Constructor
   *
   * @param EventManager $eventManager
   */
  public function __construct(
    protected EventManager $eventManager,
  )
  {
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('eventapi.event_manager'),
    );
  }

  /**
   * API endpoint to retrieve events information.
   *
   * @param string $term_name Name of the conference_year term.
   * @param Request $request Symfony http request object.
   * @return ResponseInterface Symfony framework response.
   */
  public function events(string $term_name, Request $request) {
    if (($term = $this->eventManager->searchConferenceYear($term_name)) === null) {
      $status_code = 404;
      $response = new JsonResponse([
        'method' => $request->getMethod(),
        'code' => $status_code,
        'status' => 'term not found',
      ]);
      $response->setStatusCode($status_code);
      return $response;
    }

    $options = [
      'is_admin' => $request->query->get('is_admin') === 'true',
      'lang' => $request->query->get('lang'),
    ];

    // json response
    return new JsonResponse([
      'method' => $request->getMethod(),
      'status' => 'success',
      'generated' => date('c'),
      'events' => $this->eventManager->getEvents($term, $options),
    ]);
  }

  /**
   * API endpoint to retrieve days information.
   *
   * @param string $term_name Name of the conference_year term.
   * @param Request $request Symfony http request object.
   * @return ResponseInterface Symfony framework response.
   */
  public function days(string $term_name, Request $request) {
    // if a term is not found
    if (($term = $this->eventManager->searchConferenceYear($term_name)) === null) {
      $status_code = 404;
      $response = new JsonResponse([
        'method' => $request->getMethod(),
        'code' => $status_code,
        'status' => 'term not found',
      ]);
      $response->setStatusCode($status_code);
      return $response;
    }

    // json response
    return new JsonResponse([
      'method' => $request->getMethod(),
      'status' => 'success',
      'generated' => date('c'),
      'days' => $this->eventManager->getDays($term, $request->get('lang')),
    ]);
  }

  /**
   * API endpoint to retrieve news of a event.
   *
   * @param string $term_name Name of the conference_year term.
   * @param Request $request Symfony http request object.
   * @return ResponseInterface Symfony framework response.
   */
  public function news(string $term_name, Request $request) {
    // if a term is not found
    if (($term = $this->eventManager->searchConferenceYear($term_name)) === null) {
      $status_code = 404;
      $response = new JsonResponse([
        'method' => $request->getMethod(),
        'code' => $status_code,
        'status' => 'term not found',
      ]);
      $response->setStatusCode($status_code);
      return $response;
    }

    // json response
    return new JsonResponse([
      'method' => $request->getMethod(),
      'status' => 'success',
      'generated' => date('c'),
      'news' => array_values($this->eventManager->getNews($term, $request->get('lang'))),
    ]);
  }

  /**
   * Index endpoint for API discovery
   *
   * @param Request $request
   * @return ResponseInterface Symfony framework response.
   */
  public function index(Request $request)
  {
   // generate response
    $response = new JsonResponse([
      'method' => $request->getMethod(),
      'code' => 200,
      'status' => 'success',
      'generated' => date('c'),
      'apis' => [
        'conferenceYear' => [
          'desc' => 'List all conference years, and the API endpoints relevant to them',
          'href' => Url::fromRoute('eventapi.v1.conferenceyear.list')
            ->setAbsolute(TRUE)
            ->toString(),
        ],
      ],
    ]);
    return $response;
  }

  /**
   * API endpoint to retrieve all conference years
   *
   * @param Request $request Symfony http request object.
   * @return ResponseInterface Symfony framework response.
   */
  public function conferenceYear(Request $request)
  {
    $termInfos = $this->eventManager->getConferenceYears();

    // derive the result array
    $conferenceYears = array_map(function ($termInfo) {
      return [
        'id' => $termInfo->tid,
        'uuid' => $termInfo->uuid,
        'name' => $termInfo->name,
        'eventCount' => $termInfo->eventCount,
        'depth' => $termInfo->depth,
        'apis' => [
          'info' => [
            'href' => Url::fromRoute('eventapi.v1.info', ['term_name' => $termInfo->name])
            ->setAbsolute(TRUE)
            ->toString()
          ],
          'events' => [
            'href' => Url::fromRoute('eventapi.v1.events', ['term_name' => $termInfo->name])
              ->setAbsolute(TRUE)
              ->toString()
          ],
          'days' => [
            'href' => Url::fromRoute('eventapi.v1.days', ['term_name' => $termInfo->name])
            ->setAbsolute(TRUE)
            ->toString()
          ],
        ],
      ];
    }, $termInfos);

    // generate response
    $response = new JsonResponse([
      'method' => $request->getMethod(),
      'code' => 200,
      'status' => 'success',
      'generated' => date('c'),
      'conferenceYears' => $conferenceYears,
    ]);
    return $response;
  }

  /**
   * API endpoint to get conference year information.
   *
   * @param string $term_name
   * @param Request $request
   * @return ResponseInterface Symfony framework response.
   */
  public function info(string $term_name, Request $request)
  {
    if (($term = $this->eventManager->searchConferenceYear($term_name)) === null) {
      $status_code = 404;
      $response = new JsonResponse([
        'method' => $request->getMethod(),
        'code' => $status_code,
        'status' => 'term not found',
      ]);
      $response->setStatusCode($status_code);
      return $response;
    }

    $logo_image_style = ImageStyle::load('width_550');
    $sponsors = $this->eventManager->renderSponsors(
      $term,
      [
        'premium_plus',
        'premium',
        'platinum',
        'gold',
        'silver',
        'prize',
        'venue',
        'design',
        'tool',
        'general',
      ],
      $logo_image_style,
    );

    $supportingOrgs = [];
    foreach ($term->field_supporting_organization as $fieldItem) {
      /** @var NodeInterface */
      $entity = $fieldItem->entity;
      $supportingOrgs[] = $this->eventManager->renderOrgView($entity, $logo_image_style);
    }

    // derive patron list
    $patrons = array_map(function ($patron) {
      return $patron->value;
    }, iterator_to_array($term->field_patron_names));

    return new JsonResponse([
      'method' => $request->getMethod(),
      'status' => 'success',
      'generated' => date('c'),
      'conference' => [
        'uuid' => $term->uuid(),
        'name' => $term->name->value,
        'sponsors' => $sponsors,
        'supporting_organizations' => $supportingOrgs,
        'patrons' => $patrons,
        'url' => $term->field_url?->uri  ?? '',
        'ticket_url' => $term->field_url_registration?->uri ?? '',
        'call_for_proposal_url' => $term->field_call_for_proposal_url?->uri ?? '',
      ],
    ]);
  }
}
