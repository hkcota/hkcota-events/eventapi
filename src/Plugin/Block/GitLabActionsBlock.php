<?php

namespace Drupal\eventapi\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\user\Entity\User;

/**
 * Provides a Travis Actions block.
 *
 * @Block(
 *   id = "gitlab_actions_block",
 *   admin_label = @Translation("GitLab Actions block"),
 *   category = @Translation("Admin"),
 * )
 */
class GitLabActionsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    // if user has no permission, show nothing
    if (!Drupal::currentUser()->hasPermission('execute gitlab actions')) {
      // Note: is the cache user specific, if so, there is no
      // need to prevent caching
      return [
        '#cache' => [
          'context' => ['user'],
        ],
      ];
    }

    $user = \Drupal::currentUser();
    $user = User::load($user->id());

    // check if the term is of the correct vocab
    $term = \Drupal::request()->attributes->get('taxonomy_term');
    if ($term === null || $term->bundle() != 'conference_year') {
      return [
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    }

    // check if the term has GitLab project id set
    $project_id = (string) $term->field_gitlab_repo_id->value;
    if ($project_id === '') return [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    // check if the term has GitLab pipeline triggering token
    $project_token = (string) $term->field_gitlab_project_token->value;
    if ($project_token === '') return [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    $formatted_link = new FormattableMarkup(
      '<a href="@url" target="_blank">@text</a>',
      [
        '@url' => 'https://gitlab.com/' . $project_id .'/pipelines',
        '@text' => $this->t('GitLab'),
      ]
    );

    return [
      'advanced' => [
        '#type' => 'details',
        '#title' => $this->t('Advanced Option'),
        '#open' => true,
        'form' => Drupal::formBuilder()
          ->getForm('Drupal\eventapi\Form\GitLabRebuildForm', [
            'project_id' => $project_id,
            'project_token' => $project_token,
            'term' => $term,
            'user' => $user,
          ]),
      ]
    ];
  }

}
