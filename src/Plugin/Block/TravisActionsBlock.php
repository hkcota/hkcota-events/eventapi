<?php

namespace Drupal\eventapi\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\eventapi\Form\TravisRebuildForm;

/**
 * Provides a Travis Actions block.
 *
 * @Block(
 *   id = "travis_actions_block",
 *   admin_label = @Translation("Travis Actions block"),
 *   category = @Translation("Admin"),
 * )
 */
class TravisActionsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    // if user has no permission, show nothing
    if (!Drupal::currentUser()->hasPermission('execute travis actions')) {
      // Note: is the cache user specific, if so, there is no
      // need to prevent caching
      return [
        '#cache' => [
          'context' => ['user'],
        ],
      ];
    }

    // check if the term is of the correct vocab
    $term = \Drupal::request()->attributes->get('taxonomy_term');
    if ($term === null || $term->bundle() != 'conference_year') {
      return [
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    }

    // check if the term has travis id set
    $travis_id = (string) $term->field_travis_repo_id->value;
    if ($travis_id === '') return [
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    //$rebuild_url = Url::fromRoute('eventapi.form.travis.rebuild');

    $formatted_link = new FormattableMarkup(
      '<a href="@url" target="_blank">@text</a>',
      [
        '@url' => 'https://travis-ci.org/' . $travis_id .'/builds',
        '@text' => $this->t('Travis'),
      ]
    );
    return [
      'build_results' => [
        '#markup' => new FormattableMarkup(
          '<div style="background-color: #FFE; line-height: 1.6em; padding: 1em; margin: 1.5em 0 2em; box-shadow: 1px 1px 3px #777"> <span style="color: red">*</span> ' . $this->t('Visit @link to see website build history and monitor build process.') . '</div>',
          [
            '@link' => $formatted_link
          ]
        ),
      ],
      'advanced' => [
        '#type' => 'details',
        '#title' => $this->t('Advanced Option'),
        '#open' => true,
        'message' => [
          '#markup' => '<div class="padding-bottom: 1em;">' . $this->t('You may use this form to manually trigger website rebuild') . '</div>',
        ],
        'form' => Drupal::formBuilder()
          ->getForm('Drupal\eventapi\Form\TravisRebuildForm', [
            'travis_id' => $travis_id,
          ]),
      ]
    ];
  }

}
