<?php

namespace Drupal\eventapi\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Template\Attribute;

/**
 * Provides a timetable preview for site editors.
 *
 * @Block(
 *   id = "timetable_preview_block",
 *   admin_label = @Translation("Timetable Preview block"),
 *   category = @Translation("Content"),
 * )
 */
class TimetablePreviewBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    // check if the term exists and is of the correct vocab
    $term = \Drupal::request()->attributes->get('taxonomy_term');
    if ($term === null || $term->bundle() != 'conference_year') {
      return [
        '#cache' => [
          'contexts' => ['languages', 'user', 'url.path'],
          'tags' => ['node_list'],
          'max-age' => Cache::PERMANENT,
        ],
      ];
    }

    // if the timetable is not for public
    // and user has no permission, show nothing.
    $public_viewable = (bool) ($term->field_timetable_public->value ?? 0);
    if (!$public_viewable && !Drupal::currentUser()->hasPermission('preview timetable')) {
      // Note: is the cache user specific, if so, there is no
      // need to prevent caching
      return [
        '#cache' => [
          'context' => ['user'],
          'max-age' => Cache::PERMANENT,
        ],
      ];
    }

    // cache strategy if term exists and valid
    $cache_strategy = [
      '#cache' => [
        'keys' => ['eventapi_timetable_preview', 'taxonomy', 'term', $term->id()],
        'contexts' => ['languages', 'user', 'url.path'],
        'tags' => $term->getcacheTags() + ['node_list', 'taxonomy_term_list'],
        'max-age' => 0,
      ],
    ];

    // if there is no data for the term, return array that
    // only render empty result
    $eventManager = \Drupal::service('eventapi.event_manager');
    if (empty(($days = $eventManager->getDays($term)))) {
      return $cache_strategy;
    }

    // return preview with proper cache strategy
    return $cache_strategy + [
      'preview' => [
        '#theme' => 'eventapi_timetable_preview',
        '#attached' => [
          'library' => [
            'eventapi/timetable',
          ],
        ],
        '#wrapper_attributes' => new Attribute([
          'class' => [
            'timetable',
          ],
        ]),
        '#days' => $days,
        '#conference' => $term,
      ],
    ];
  }

  public function getCacheContexts()
  {
    return ['languages', 'user', 'url.path'];
  }

  public function getCacheTags()
  {
    // check if the term exists and is of the correct vocab
    $term = \Drupal::request()->attributes->get('taxonomy_term');
    if ($term === null || $term->bundle() != 'conference_year') {
      return ['node_list'];
    }
    return $term->getcacheTags() + ['node_list', 'taxonomy_term_list', ''];
  }

  public function getCacheMaxAge()
  {
    return 0;
  }
}
